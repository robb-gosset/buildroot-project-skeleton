BUILDIR		= $(CURDIR)/build
SRCDIR		= $(CURDIR)/src
BOARD		= IGEPv2

.PHONY: setup config source build

setup:
	git submodule update --init

config: $(BUILDIR)/.config

source: configs
	$(MAKE) -C $(BUILDIR) BR2_DL_DIR=$(SRCDIR) source

build: source
	$(MAKE) -C $(BUILDIR) BR2_DL_DIR=$(SRCDIR)

$(BUILDIR)/.config:
	mkdir -p $(BUILDIR)
	$(MAKE) O=$(BUILDIR) -C buildroot BR2_EXTERNAL=$(CURDIR)/buildroot-external BR2_DL_DIR=$(SRCDIR) $(BOARD)_defconfig

menuconfig:
	$(MAKE) -C $(BUILDIR) menuconfig

savedefconfig:
	$(MAKE) -C $(BUILDIR) savedefconfig BR2_DEFCONFIG=$(CURDIR)/buildroot-external/configs/$(BOARD)_defconfig

